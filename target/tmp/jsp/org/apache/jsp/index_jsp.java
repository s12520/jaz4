package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class index_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.Vector _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public Object getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html; charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">\n");
      out.write("<html>\n");
      out.write("<head>\n");
      out.write("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n");
      out.write("<title>Pokaż Pogodę</title>\n");
      out.write("</head>\n");
      out.write("<body>\n");
      domain.City city = null;
      synchronized (session) {
        city = (domain.City) _jspx_page_context.getAttribute("city", PageContext.SESSION_SCOPE);
        if (city == null){
          city = new domain.City();
          _jspx_page_context.setAttribute("city", city, PageContext.SESSION_SCOPE);
        }
      }
      out.write('\n');
      service.WeatherService apiService = null;
      synchronized (application) {
        apiService = (service.WeatherService) _jspx_page_context.getAttribute("apiService", PageContext.APPLICATION_SCOPE);
        if (apiService == null){
          apiService = new service.WeatherService();
          _jspx_page_context.setAttribute("apiService", apiService, PageContext.APPLICATION_SCOPE);
        }
      }
      out.write("\n");
      out.write("\n");
      out.write("Wybierz miasto w kótrym chcesz sprawdzić pogodę: \n");
      out.write("\n");
      out.write(" <form action=\"showWeather.jsp\">\n");
      out.write("  <input type=\"radio\" name=\"city\" id=\"city_name\" value=\"Warszawa\" checked> Warszawa<br>\n");
      out.write("  <input type=\"radio\" name=\"city\" id=\"city_name\" value=\"Gdansk\"> Gdańsk<br>\n");
      out.write("  <input type=\"radio\" name=\"city\" id=\"city_name\" value=\"Krakow\"> Kraków<br>\n");
      out.write("  <input type=\"radio\" name=\"city\" id=\"city_name\" value=\"Wroclaw\"> Wrocław<br>\n");
      out.write("  <input type=\"radio\" name=\"city\" id=\"city_name\" value=\"Poznan\"> Poznań<br>\n");
      out.write("  <input type=\"radio\" name=\"city\" id=\"city_name\" value=\"Lodz\"> Łódź<br>\n");
      out.write("  <input type=\"radio\" name=\"city\" id=\"city_name\" value=\"Katowice\"> Katowice<br>\n");
      out.write("  <input type=\"submit\" value=\"Submit\">\n");
      out.write("</form> \n");
      out.write("\n");
      out.write("\n");
      out.write("</body>\n");
      out.write("</html>\n");
      out.write("\t");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
