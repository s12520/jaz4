package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import domain.Person;

public final class showAllPersons_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.Vector _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public Object getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html; charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\r\n");
      out.write("\r\n");
      out.write("<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">\r\n");
      out.write("<html>\r\n");
      out.write("<head>\r\n");
      out.write("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\r\n");
      out.write("<title>Insert title here</title>\r\n");
      out.write("</head>\r\n");
      out.write("<body>\r\n");
      out.write("\r\n");
      domain.City city = null;
      synchronized (session) {
        city = (domain.City) _jspx_page_context.getAttribute("city", PageContext.SESSION_SCOPE);
        if (city == null){
          city = new domain.City();
          _jspx_page_context.setAttribute("city", city, PageContext.SESSION_SCOPE);
        }
      }
      out.write('\r');
      out.write('\n');
      org.apache.jasper.runtime.JspRuntimeLibrary.introspecthelper(_jspx_page_context.findAttribute("city"), "city_name", request.getParameter("city"), request, "city", false);
      out.write('\r');
      out.write('\n');
      service.PeopleApiService apiService = null;
      synchronized (application) {
        apiService = (service.PeopleApiService) _jspx_page_context.getAttribute("apiService", PageContext.APPLICATION_SCOPE);
        if (apiService == null){
          apiService = new service.PeopleApiService();
          _jspx_page_context.setAttribute("apiService", apiService, PageContext.APPLICATION_SCOPE);
        }
      }
      out.write("\r\n");
      out.write("Wybrales: ");
      out.write(org.apache.jasper.runtime.JspRuntimeLibrary.toString((((domain.City)_jspx_page_context.findAttribute("city")).getCity_name())));
      out.write("\r\n");
      out.write("\r\n");

String miasto = city.getCity_name();
out.println("<p>"+"Temperatura: "+apiService.main(miasto).getTemperature()+" C</p><p>" +
				  "Pogoda: "+apiService.main(miasto).getWeather_main()+"</p><p>" +
				  "rodzaj:"+apiService.main(miasto).getWeather_description()+"</p><p>" +
				  "zagęszczenie chmór: "+apiService.main(miasto).getClouds_dt()+" %</p><p>" +
				  "Ciśnienie: "+apiService.main(miasto).getPressure()+" hPa</p><p>" +
				  "Prędkość Wiatru: "+apiService.main(miasto).getWind_speed()+" metry/sek"	 );




      out.write("\r\n");
      out.write("<p>\r\n");
      out.write("  <a href=\"PersonForm.jsp\">Add another person</a>\r\n");
      out.write("</p>\r\n");
      out.write("\r\n");
      out.write("</body>\r\n");
      out.write("</html>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
