package service;

import java.io.IOException;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import domain.City;


public class WeatherService {


    public static City main(String city1) {
    
    	String appid = "ccf03697938efa9550037c16918e5ed6";
    	
    	final String URL_OpenWeatherMap_weather_London_uk =
                "http://api.openweathermap.org/data/2.5/weather?q="+city1+",pl&appid="+appid;
     	
    	
    	City city_1 = new City();
        String result = "";
         
        try {
            URL url_weather = new URL(URL_OpenWeatherMap_weather_London_uk);
 
            HttpURLConnection httpURLConnection = (HttpURLConnection) url_weather.openConnection();
 
            if (httpURLConnection.getResponseCode() == HttpURLConnection.HTTP_OK) {
 
                InputStreamReader inputStreamReader =
                    new InputStreamReader(httpURLConnection.getInputStream());
                BufferedReader bufferedReader =
                    new BufferedReader(inputStreamReader, 8192);
                String line = null;
                while((line = bufferedReader.readLine()) != null){
                    result += line;
                }
                 
                bufferedReader.close();
                 	      
    	        JSONObject jsonObject = new JSONObject(result);
    	 
    	      
    	        //"weather"
    	       
    	        JSONArray JSONArray_weather = jsonObject.getJSONArray("weather");
    	        if(JSONArray_weather.length() > 0){
    	            JSONObject JSONObject_weather = JSONArray_weather.getJSONObject(0);
    	            
    	            city_1.setWeather_main(JSONObject_weather.getString("main"));         
    	            city_1.setWeather_description(JSONObject_weather.getString("description")); 
    	          
    	        }
    	             	  
    	        //"main"
    	        JSONObject JSONObject_main = jsonObject.getJSONObject("main");
    	        
    	        	city_1.setTemperature(JSONObject_main.getDouble("temp")-273.15); 
    	        	city_1.setPressure(JSONObject_main.getDouble("pressure"));
    	        
    	        //"wind"
    	        JSONObject JSONObject_wind = jsonObject.getJSONObject("wind");  
    	        	city_1.setWind_speed(JSONObject_wind.getDouble("speed"));
    	     
    	        //"clouds"
    	        JSONObject JSONObject_clouds = jsonObject.getJSONObject("clouds");
    	    
    	        	city_1.setClouds_dt(JSONObject_clouds.getInt("all")); 
    	      
 
            } else {
                System.out.println("Error in httpURLConnection.getResponseCode()!!!");
            }
 
        } catch (MalformedURLException ex) {
            Logger.getLogger(WeatherService.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(WeatherService.class.getName()).log(Level.SEVERE, null, ex);
        } catch (JSONException ex) {
            Logger.getLogger(WeatherService.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return city_1;
    }
	
}