<%@page import="domain.City"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Pogoda</title>
</head>
<body>

<jsp:useBean id="city" class="domain.City" scope="session" />
<jsp:setProperty name="city" property="city_name" param="city"/>
<jsp:useBean id="apiService" class="service.WeatherService" scope="application" />
Wybrales: <jsp:getProperty name="city" property="city_name"/>

<%
String miasto = city.getCity_name();
out.println("<p>"+"Temperatura: "+apiService.main(miasto).getTemperature()+" C</p><p>" +
				  "Pogoda: "+apiService.main(miasto).getWeather_main()+"</p><p>" +
				  "rodzaj: "+apiService.main(miasto).getWeather_description()+"</p><p>" +
				  "zagęszczenie chmór: "+apiService.main(miasto).getClouds_dt()+" %</p><p>" +
				  "Ciśnienie: "+apiService.main(miasto).getPressure()+" hPa</p><p>" +
				  "Prędkość Wiatru: "+apiService.main(miasto).getWind_speed()+" metry/sek"	 );
%>
<p>
  <a href="index.jsp">Sprawdź pogodę w innym mieście</a>
</p>

</body>
</html>